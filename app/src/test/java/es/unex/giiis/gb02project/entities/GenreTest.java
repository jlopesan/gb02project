package es.unex.giiis.gb02project.entities;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.Genre;

import static org.junit.Assert.*;

public class GenreTest {

    private Genre genre;

    @Before
    public void setUp() throws Exception {
        genre = new Genre(1, "texto");
    }

    @Test
    public void getIDGenreTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = genre.getClass().getDeclaredField("mIDGenre");
        field.setAccessible(true);
        int value = 1;
        field.set(genre, value);


        //when
        final int result = genre.getIDGenre();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setIDGenreTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        genre.setIDGenre(value);
        final Field field = genre.getClass().getDeclaredField("mIDGenre");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(genre), value);
    }

    @Test
    public void getNameTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = genre.getClass().getDeclaredField("mName");
        field.setAccessible(true);
        String value = "texto";
        field.set(genre, value);


        //when
        final String result = genre.getName();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setNameTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        genre.setName(value);
        final Field field = genre.getClass().getDeclaredField("mName");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(genre), value);
    }

    @Test
    public void packageIntentTest() throws NoSuchFieldException, IllegalAccessException {

    }
}