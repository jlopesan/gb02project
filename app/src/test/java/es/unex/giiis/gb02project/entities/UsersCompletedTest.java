package es.unex.giiis.gb02project.entities;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PopularGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;

import static org.junit.Assert.*;

public class UsersCompletedTest {

    @Test
    public void getIDGame() throws NoSuchFieldException, IllegalAccessException{
        UsersCompleted instance = new UsersCompleted(1,1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");
        final Field field = instance.getClass().getDeclaredField("mIDGame");
        field.setAccessible(true);
        field.set(instance, new Integer(1));
        final Integer result = instance.getIDGame();
        assertEquals("Los campos no coinciden", result, new Integer(1));
    }

    @Test
    public void getName() throws NoSuchFieldException, IllegalAccessException{
        UsersCompleted instance = new UsersCompleted(1,1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");
        final Field field = instance.getClass().getDeclaredField("mName");
        field.setAccessible(true);
        field.set(instance, "Iron man");
        final String result = instance.getName();
        assertEquals("Los campos no coinciden", result, "Iron man");
    }

    @Test
    public void getBackgroundImage() throws NoSuchFieldException, IllegalAccessException{
        UsersCompleted instance = new UsersCompleted(1,1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");
        final Field field = instance.getClass().getDeclaredField("mBackgroundImage");
        field.setAccessible(true);
        field.set(instance, "lucha");
        final String result = instance.getBackgroundImage();
        assertEquals("Los campos no coinciden", result, "lucha");
    }

    @Test
    public void getRating() throws NoSuchFieldException, IllegalAccessException{
        UsersCompleted instance = new UsersCompleted(1,1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");
        final Field field = instance.getClass().getDeclaredField("mRating");
        field.setAccessible(true);
        field.set(instance, new Double(3.5));
        final Double result = instance.getRating();
        assertEquals("Los campos no coinciden", result, new Double(3.5));
    }

    @Test
    public void getDescription() throws NoSuchFieldException, IllegalAccessException{
        UsersCompleted instance = new UsersCompleted(1,1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");
        final Field field = instance.getClass().getDeclaredField("mDescription");
        field.setAccessible(true);
        field.set(instance, "Es un juego muy divertido");
        final String result = instance.getDescription();
        assertEquals("Los campos no coinciden", result, "Es un juego muy divertido");
    }

    @Test
    public void getBackground_image_additional() throws NoSuchFieldException, IllegalAccessException{
        UsersCompleted instance = new UsersCompleted(1,1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");
        final Field field = instance.getClass().getDeclaredField("mBackground_image_additional");
        field.setAccessible(true);
        field.set(instance, "final");
        final String result = instance.getBackground_image_additional();
        assertEquals("Los campos no coinciden", result, "final");
    }

    @Test
    public void setIDUser() throws NoSuchFieldException, IllegalAccessException{
        UsersCompleted instance = new UsersCompleted(1,1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");
        Long value = new Long(1) ;
        instance.setIDUser(value);
        final Field field = instance.getClass().getDeclaredField("mIDUser");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void setIDGame() throws NoSuchFieldException, IllegalAccessException{
        UsersCompleted instance = new UsersCompleted(1,1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");
        Integer value = 1;
        instance.setIDGame(value);
        final Field field = instance.getClass().getDeclaredField("mIDGame");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void setName() throws NoSuchFieldException, IllegalAccessException{
        UsersCompleted instance = new UsersCompleted(1,1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");
        String value = "Iron Man";
        instance.setName(value);
        final Field field = instance.getClass().getDeclaredField("mName");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void setBackgroundImage() throws NoSuchFieldException, IllegalAccessException{
        UsersCompleted instance = new UsersCompleted(1,1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");
        String value = "lucha";
        instance.setBackgroundImage(value);
        final Field field = instance.getClass().getDeclaredField("mBackgroundImage");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void setRating() throws NoSuchFieldException, IllegalAccessException{
        final PopularGames instance = new PopularGames(1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");
        Double value = 3.5;
        instance.setRating(value);
        final Field field = instance.getClass().getDeclaredField("mRating");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);

    }

    @Test
    public void setDescription() throws NoSuchFieldException, IllegalAccessException{
        UsersCompleted instance = new UsersCompleted(1,1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");
        String value = "Es un juego muy divertido";
        instance.setDescription(value);
        final Field field = instance.getClass().getDeclaredField("mDescription");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void setBackground_image_additional() throws NoSuchFieldException, IllegalAccessException{
        UsersCompleted instance = new UsersCompleted(1,1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");
        String value = "final";
        instance.setBackgroundImage(value);
        final Field field = instance.getClass().getDeclaredField("mBackgroundImage");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void toResult() throws NoSuchFieldException, IllegalAccessException{
        final UsersCompleted instance = new UsersCompleted(1,1,"Iron man","lucha",3.5,"Es un juego muy divertido","final");

        final Field field0 = instance.getClass().getDeclaredField("mIDUser");
        field0.setAccessible(true);
        field0.set(instance, new Long(1));

        final Field field1 = instance.getClass().getDeclaredField("mIDGame");
        field1.setAccessible(true);
        field1.set(instance, new Integer(1));

        final Field field2 = instance.getClass().getDeclaredField("mName");
        field2.setAccessible(true);
        field2.set(instance, "text");

        final Field field3 = instance.getClass().getDeclaredField("mBackgroundImage");
        field3.setAccessible(true);
        field3.set(instance, "text");

        final Field field4 = instance.getClass().getDeclaredField("mBackground_image_additional");
        field4.setAccessible(true);
        field4.set(instance, "text");

        final Field field5 = instance.getClass().getDeclaredField("mRating");
        field5.setAccessible(true);
        field5.set(instance, new Double(3.5));

        final Field field6 = instance.getClass().getDeclaredField("mDescription");
        field6.setAccessible(true);
        field6.set(instance, "text");

        Result result = instance.toResult();

        assertEquals("field wasn't retrieved properly", result.getId(), new Integer(1));
        assertEquals("field wasn't retrieved properly", result.getName(), "text");
        assertEquals("field wasn't retrieved properly", result.getDescription(), "text");
        assertEquals("field wasn't retrieved properly", result.getBackgroundImage(), "text");
        assertEquals("field wasn't retrieved properly", result.getBackgroundImageAdditional(), "text");
        assertEquals("field wasn't retrieved properly", result.getRating(), new Double(3.5));

    }

}