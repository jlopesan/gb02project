package es.unex.giiis.gb02project.model;

import android.icu.text.CaseMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Rating;

import static org.junit.Assert.*;

public class RatingTest {



    @Test
    public void getId() throws NoSuchFieldException, IllegalAccessException{
        final Rating instance = new Rating();
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, new Integer(1));

        final Integer result = instance.getId();
        assertEquals("Los campos no coinciden", result, new Integer(1));

    }

    @Test
    public void setId() throws NoSuchFieldException, IllegalAccessException{
        final Rating instance = new Rating();
        Integer value = 1;
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void getTitle() throws NoSuchFieldException, IllegalAccessException{
        final Rating instance = new Rating();
        final Field field = instance.getClass().getDeclaredField("title");
        field.setAccessible(true);
        field.set(instance, "Fortnite");
        final String result = instance.getTitle();
        assertEquals("Los campos no coinciden", result, "Fortnite");
    }

    @Test
    public void setTitle() throws NoSuchFieldException, IllegalAccessException{
        String value = "Fortnite";
        final Rating instance = new Rating();
        instance.setTitle(value);
        final Field field = instance.getClass().getDeclaredField("title");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void getCount() throws NoSuchFieldException, IllegalAccessException{
        final Rating instance = new Rating();
        final Field field = instance.getClass().getDeclaredField("count");
        field.setAccessible(true);
        field.set(instance, new Integer(1));

        final Integer result = instance.getCount();
        assertEquals("Los campos no coinciden", result, new Integer(1));
    }

    @Test
    public void setCount() throws NoSuchFieldException, IllegalAccessException{
        final Rating instance = new Rating();
        Integer value = 1;
        instance.setCount(value);
        final Field field = instance.getClass().getDeclaredField("count");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void getPercent() throws NoSuchFieldException, IllegalAccessException{
        final Rating instance = new Rating();
        final Field field = instance.getClass().getDeclaredField("percent");
        field.setAccessible(true);
        field.set(instance, new Double(1.5));

        final Double result = instance.getPercent();
        assertEquals("Los campos no coinciden", result, new Double(1.5));
    }

    @Test
    public void setPercent() throws NoSuchFieldException, IllegalAccessException{
        final Rating instance = new Rating();
        Double value = 1.5;
        instance.setPercent(value);
        final Field field = instance.getClass().getDeclaredField("percent");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }
}