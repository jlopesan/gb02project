package es.unex.giiis.gb02project.model;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Tag;

import static org.junit.Assert.*;

public class TagTest {

    @Test
    public void getIdTest() throws NoSuchFieldException, IllegalAccessException  {
        final Tag instance = new Tag();
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, 1);
        //when
        final Integer result = instance.getId();
        //then
        assertEquals("field wasn't retrieved properly", result.intValue(), 1);
    }

    @Test
    public void setIdTest() throws NoSuchFieldException, IllegalAccessException  {
        Integer value = 1;
        Tag instance = new Tag();
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getNameTest() throws NoSuchFieldException, IllegalAccessException  {
        final Tag instance = new Tag();
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        field.set(instance, "NameExample");
        //when
        final String result = instance.getName();
        //then
        assertEquals("field wasn't retrieved properly", result, "NameExample");
    }

    @Test
    public void setNameTest() throws NoSuchFieldException, IllegalAccessException  {
        String value = "NameExample";
        Tag instance = new Tag();
        instance.setName(value);
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getSlugTest() throws NoSuchFieldException, IllegalAccessException  {
        final Tag instance = new Tag();
        final Field field = instance.getClass().getDeclaredField("slug");
        field.setAccessible(true);
        field.set(instance, "SlugExample");
        //when
        final String result = instance.getSlug();
        //then
        assertEquals("field wasn't retrieved properly", result, "SlugExample");
    }

    @Test
    public void setSlugTest() throws NoSuchFieldException, IllegalAccessException  {
        String value = "SlugExample";
        Tag instance = new Tag();
        instance.setSlug(value);
        final Field field = instance.getClass().getDeclaredField("slug");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getLanguageTest() throws NoSuchFieldException, IllegalAccessException  {
        final Tag instance = new Tag();
        final Field field = instance.getClass().getDeclaredField("language");
        field.setAccessible(true);
        field.set(instance, "LanguageExample");
        //when
        final String result = instance.getLanguage();
        //then
        assertEquals("field wasn't retrieved properly", result, "LanguageExample");
    }

    @Test
    public void setLanguageTest() throws NoSuchFieldException, IllegalAccessException  {
        String value = "LanguageExample";
        Tag instance = new Tag();
        instance.setLanguage(value);
        final Field field = instance.getClass().getDeclaredField("language");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getGamesCountTest() throws NoSuchFieldException, IllegalAccessException  {
        final Tag instance = new Tag();
        final Field field = instance.getClass().getDeclaredField("gamesCount");
        field.setAccessible(true);
        field.set(instance, 1);
        //when
        final Integer result = instance.getGamesCount();
        //then
        assertEquals("field wasn't retrieved properly", result.intValue(), 1);
    }

    @Test
    public void setGamesCountTest() throws NoSuchFieldException, IllegalAccessException  {
        Integer value = 1;
        Tag instance = new Tag();
        instance.setGamesCount(value);
        final Field field = instance.getClass().getDeclaredField("gamesCount");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getImageBackgroundTest() throws NoSuchFieldException, IllegalAccessException  {
        final Tag instance = new Tag();
        final Field field = instance.getClass().getDeclaredField("imageBackground");
        field.setAccessible(true);
        field.set(instance, "ImageBackgroundExample");
        //when
        final String result = instance.getImageBackground();
        //then
        assertEquals("field wasn't retrieved properly", result, "ImageBackgroundExample");
    }

    @Test
    public void setImageBackgroundTest() throws NoSuchFieldException, IllegalAccessException  {
        String value = "ImageBackgroundExample";
        Tag instance = new Tag();
        instance.setImageBackground(value);
        final Field field = instance.getClass().getDeclaredField("imageBackground");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }
}