package es.unex.giiis.gb02project.model;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.AddedByStatus;

import static org.junit.Assert.assertEquals;

public class AddedByStatusTest {

    private AddedByStatus addedByStatus;

    @Before
    public void setUp() throws Exception {
        addedByStatus = new AddedByStatus();

    }

    @Test
    public void getYetTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = addedByStatus.getClass().getDeclaredField("yet");
        field.setAccessible(true);
        field.set(addedByStatus, 1);

        //when
        final int result = addedByStatus.getYet();

        //then
        assertEquals("field wasn't retrieved properly", result, 1);
    }

    @Test
    public void setYetTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        addedByStatus.setYet(value);
        final Field field = addedByStatus.getClass().getDeclaredField("yet");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(addedByStatus), value);
    }

    @Test
    public void getOwnedTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = addedByStatus.getClass().getDeclaredField("owned");
        field.setAccessible(true);
        field.set(addedByStatus, 1);

        //when
        final int result = addedByStatus.getOwned();

        //then
        assertEquals("field wasn't retrieved properly", result, 1);
    }

    @Test
    public void setOwnedTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        addedByStatus.setOwned(value);
        final Field field = addedByStatus.getClass().getDeclaredField("owned");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(addedByStatus), value);
    }

    @Test
    public void getBeatenTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = addedByStatus.getClass().getDeclaredField("beaten");
        field.setAccessible(true);
        field.set(addedByStatus, 1);

        //when
        final int result = addedByStatus.getBeaten();

        //then
        assertEquals("field wasn't retrieved properly", result, 1);
    }

    @Test
    public void setBeatenTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        addedByStatus.setBeaten(value);
        final Field field = addedByStatus.getClass().getDeclaredField("beaten");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(addedByStatus), value);
    }

    @Test
    public void getToplayTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = addedByStatus.getClass().getDeclaredField("toplay");
        field.setAccessible(true);
        field.set(addedByStatus, 1);

        //when
        final int result = addedByStatus.getToplay();

        //then
        assertEquals("field wasn't retrieved properly", result, 1);
    }

    @Test
    public void setToplayTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        addedByStatus.setToplay(value);
        final Field field = addedByStatus.getClass().getDeclaredField("toplay");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(addedByStatus), value);
    }

    @Test
    public void getDroppedTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = addedByStatus.getClass().getDeclaredField("dropped");
        field.setAccessible(true);
        field.set(addedByStatus, 1);

        //when
        final int result = addedByStatus.getDropped();

        //then
        assertEquals("field wasn't retrieved properly", result, 1);
    }

    @Test
    public void setDroppedTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        addedByStatus.setDropped(value);
        final Field field = addedByStatus.getClass().getDeclaredField("dropped");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(addedByStatus), value);
    }

    @Test
    public void getPlayingTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = addedByStatus.getClass().getDeclaredField("playing");
        field.setAccessible(true);
        field.set(addedByStatus, 1);

        //when
        final int result = addedByStatus.getPlaying();

        //then
        assertEquals("field wasn't retrieved properly", result, 1);
    }

    @Test
    public void setPlayingTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        addedByStatus.setPlaying(value);
        final Field field = addedByStatus.getClass().getDeclaredField("playing");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(addedByStatus), value);
    }
}