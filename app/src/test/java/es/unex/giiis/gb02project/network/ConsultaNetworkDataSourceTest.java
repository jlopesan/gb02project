package es.unex.giiis.gb02project.network;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.checkerframework.checker.units.qual.C;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.gb02project.dataApp.network.ConsultaNetworkDataSource;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Genre;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PlatformCategory;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConsultaNetworkDataSourceTest {
    @Mock
    private ConsultaNetworkDataSource consultaNetworkDataSource;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() throws Exception {
        consultaNetworkDataSource = ConsultaNetworkDataSource.getInstance();
        MockitoAnnotations.initMocks(this);

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void shouldGetInstance() {
        assertNotNull(consultaNetworkDataSource);
    }

    @Test
    public void shouldGetCurrentPlatformCategories() {
        final MutableLiveData<List<PlatformCategory>> mDownloadedPlatformCategories = new MutableLiveData<>();
        List<PlatformCategory> pcl=new ArrayList<>();

        PlatformCategory pc1 = new PlatformCategory(1, "Categoria 1");
        PlatformCategory pc2 = new PlatformCategory(2, "Categoria 2");

        pcl.add(pc1);
        pcl.add(pc2);

        mDownloadedPlatformCategories.setValue(pcl);

        LiveData<List<PlatformCategory>> ldpc = mDownloadedPlatformCategories;

        when(consultaNetworkDataSource.getCurrentPlatformCategories()).thenReturn(ldpc);

        assertEquals(consultaNetworkDataSource.getCurrentPlatformCategories().getValue().get(0).getIDPlatform(), pc1.getIDPlatform());
        assertEquals(consultaNetworkDataSource.getCurrentPlatformCategories().getValue().get(0).getName(), pc1.getName());


        assertEquals(consultaNetworkDataSource.getCurrentPlatformCategories().getValue().get(1).getIDPlatform(), pc2.getIDPlatform());
        assertEquals(consultaNetworkDataSource.getCurrentPlatformCategories().getValue().get(1).getName(), pc2.getName());
    }

    @Test
    public void shouldGetCurrentGenres() {
        final MutableLiveData<List<Genre>> mDownloadedGenre = new MutableLiveData<>();
        List<Genre> gl=new ArrayList<>();

        Genre g1 = new Genre(1, "Genre 1");
        Genre g2 = new Genre(2, "Genre 2");

        gl.add(g1);
        gl.add(g2);

        mDownloadedGenre.setValue(gl);

        LiveData<List<Genre>> ldg = mDownloadedGenre;

        when(consultaNetworkDataSource.getCurrentGenres()).thenReturn(ldg);

        assertEquals(consultaNetworkDataSource.getCurrentGenres().getValue().get(0).getIDGenre(), g1.getIDGenre());
        assertEquals(consultaNetworkDataSource.getCurrentGenres().getValue().get(0).getName(), g1.getName());


        assertEquals(consultaNetworkDataSource.getCurrentGenres().getValue().get(1).getIDGenre(), g2.getIDGenre());
        assertEquals(consultaNetworkDataSource.getCurrentGenres().getValue().get(1).getName(), g2.getName());
    }
}