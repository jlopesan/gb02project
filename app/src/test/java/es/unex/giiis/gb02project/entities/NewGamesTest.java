package es.unex.giiis.gb02project.entities;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.NewGames;

import static org.junit.Assert.*;

public class NewGamesTest {

    private NewGames instance;

    @Before
    public void setUp() throws Exception {
        instance = new NewGames(1, "mName", "mBackgroundImage", 4.5, "mDescription",  "mBackground_image_additional");
    }

    @Test
    public void setID() throws NoSuchFieldException, IllegalAccessException {
        Integer value = 1;
        instance.setID(value);
        final Field field = instance.getClass().getDeclaredField("mID");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setName() throws NoSuchFieldException, IllegalAccessException {
        String value = "text";
        instance.setName(value);
        final Field field = instance.getClass().getDeclaredField("mName");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setBackgroundImage() throws NoSuchFieldException, IllegalAccessException {
        String value = "text";
        instance.setBackgroundImage(value);
        final Field field = instance.getClass().getDeclaredField("mBackgroundImage");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setRating() throws NoSuchFieldException, IllegalAccessException {
        Double value = 3.5;
        instance.setRating(value);
        final Field field = instance.getClass().getDeclaredField("mRating");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setDescription() throws NoSuchFieldException, IllegalAccessException {
        String value = "text";
        instance.setDescription(value);
        final Field field = instance.getClass().getDeclaredField("mDescription");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setBackground_image_additional() throws NoSuchFieldException, IllegalAccessException {
        String value = "text";
        instance.setBackground_image_additional(value);
        final Field field = instance.getClass().getDeclaredField("mBackground_image_additional");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getID() throws IllegalAccessException, NoSuchFieldException {
        final Field field = instance.getClass().getDeclaredField("mID");
        field.setAccessible(true);
        field.set(instance, new Integer(1));
        //when
        final Integer result = instance.getID();
        //then
        assertEquals("field wasn't retrieved properly", result, new Integer(1));
    }

    @Test
    public void getName() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("mName");
        field.setAccessible(true);
        field.set(instance, "text");
        //when
        final String result = instance.getName();
        //then
        assertEquals("field wasn't retrieved properly", "text",result );
    }

    @Test
    public void getBackgroundImage() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("mBackgroundImage");
        field.setAccessible(true);
        field.set(instance, "text");
        //when
        final String result = instance.getBackgroundImage();
        //then
        assertEquals("field wasn't retrieved properly", result, "text");
    }

    @Test
    public void getRating() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("mRating");
        field.setAccessible(true);
        field.set(instance, new Double(3.5));
        //when
        final Double result = instance.getRating();
        //then
        assertEquals("field wasn't retrieved properly", result, new Double(3.5));
    }

    @Test
    public void getDescription() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("mDescription");
        field.setAccessible(true);
        field.set(instance, "text");
        //when
        final String result = instance.getDescription();
        //then
        assertEquals("field wasn't retrieved properly", result, "text");
    }

    @Test
    public void getBackground_image_additional() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("mBackground_image_additional");
        field.setAccessible(true);
        field.set(instance, "text");
        //when
        final String result = instance.getBackground_image_additional();
        //then
        assertEquals("field wasn't retrieved properly", result, "text");
    }

    @Test
    public void packageIntent() {
    }

    @Test
    public void toResult() throws NoSuchFieldException, IllegalAccessException {
        final Field field1 = instance.getClass().getDeclaredField("mID");
        field1.setAccessible(true);
        field1.set(instance, new Integer(1));

        final Field field2 = instance.getClass().getDeclaredField("mName");
        field2.setAccessible(true);
        field2.set(instance, "text");

        final Field field3 = instance.getClass().getDeclaredField("mBackground_image_additional");
        field3.setAccessible(true);
        field3.set(instance, "text");

        final Field field4 = instance.getClass().getDeclaredField("mBackgroundImage");
        field4.setAccessible(true);
        field4.set(instance, "text");

        final Field field5 = instance.getClass().getDeclaredField("mRating");
        field5.setAccessible(true);
        field5.set(instance, new Double(3.5));

        final Field field6 = instance.getClass().getDeclaredField("mDescription");
        field6.setAccessible(true);
        field6.set(instance, "text");

        Result result = instance.toResult();

        assertEquals("field wasn't retrieved properly", result.getId(), new Integer(1));
        assertEquals("field wasn't retrieved properly", result.getName(), "text");
        assertEquals("field wasn't retrieved properly", result.getDescription(), "text");
        assertEquals("field wasn't retrieved properly", result.getBackgroundImage(), "text");
        assertEquals("field wasn't retrieved properly", result.getBackgroundImageAdditional(), "text");
        assertEquals("field wasn't retrieved properly", result.getRating(), new Double(3.5));
    }

}