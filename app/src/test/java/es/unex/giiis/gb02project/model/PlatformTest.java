package es.unex.giiis.gb02project.model;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Platform;
import es.unex.giiis.gb02project.dataApp.model.Platform_;

import static org.junit.Assert.*;

public class PlatformTest {

    private Platform instance;

    @Before
    public void setUp() throws Exception {
        instance = new Platform();
    }

    @Test
    public void getPlatform() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("platform");
        field.setAccessible(true);
        Platform_ value = new Platform_();
        field.set(instance, value);
        //when
        final Platform_ result = instance.getPlatform();
        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setPlatform() throws NoSuchFieldException, IllegalAccessException {
        Platform_ value = new Platform_();
        instance.setPlatform(value);
        final Field field = instance.getClass().getDeclaredField("platform");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getReleasedAt() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("releasedAt");
        field.setAccessible(true);
        field.set(instance, "text");
        //when
        final String result = instance.getReleasedAt();
        //then
        assertEquals("field wasn't retrieved properly", result, "text");
    }

    @Test
    public void setReleasedAt() throws NoSuchFieldException, IllegalAccessException {
        String value = "text";
        instance.setReleasedAt(value);
        final Field field = instance.getClass().getDeclaredField("releasedAt");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getRequirementsEn() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("requirementsEn");
        field.setAccessible(true);
        Object value = new Object();
        field.set(instance, value);
        //when
        final Object result = instance.getRequirementsEn();
        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setRequirementsEn() throws NoSuchFieldException, IllegalAccessException {
        Object value = new Object();
        instance.setRequirementsEn(value);
        final Field field = instance.getClass().getDeclaredField("requirementsEn");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getRequirementsRu() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("requirementsRu");
        field.setAccessible(true);
        Object value = new Object();
        field.set(instance, value);
        //when
        final Object result = instance.getRequirementsRu();
        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setRequirementsRu() throws NoSuchFieldException, IllegalAccessException {
        Object value = new Object();
        instance.setRequirementsRu(value);
        final Field field = instance.getClass().getDeclaredField("requirementsRu");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }
}