package es.unex.giiis.gb02project.model;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.gb02project.dataApp.model.Consulta;
import es.unex.giiis.gb02project.dataApp.model.Filters;
import es.unex.giiis.gb02project.dataApp.model.Result;

import static org.junit.Assert.assertEquals;

public class ConsultaTest {

    private Consulta consulta;

    @Before
    public void setUp() throws Exception {
        consulta = new Consulta();
    }

    @Test
    public void getCountTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = consulta.getClass().getDeclaredField("count");
        field.setAccessible(true);
        int value = 1;
        field.set(consulta, value);


        //when
        final int result = consulta.getCount();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setCountTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        consulta.setCount(value);
        final Field field = consulta.getClass().getDeclaredField("count");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(consulta), value);
    }

    @Test
    public void getNextTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = consulta.getClass().getDeclaredField("next");
        field.setAccessible(true);
        String value = "texto";
        field.set(consulta, value);


        //when
        final String result = consulta.getNext();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setNextTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        consulta.setNext(value);
        final Field field = consulta.getClass().getDeclaredField("next");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(consulta), value);
    }

    @Test
    public void getPreviousTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = consulta.getClass().getDeclaredField("previous");
        field.setAccessible(true);
        Object value = new Object();
        field.set(consulta, value);


        //when
        final Object result = consulta.getPrevious();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setPreviousTest() throws NoSuchFieldException, IllegalAccessException {
        Object value = new Object ();

        consulta.setPrevious(value);
        final Field field = consulta.getClass().getDeclaredField("previous");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(consulta), value);
    }

    @Test
    public void getResultsTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = consulta.getClass().getDeclaredField("results");
        field.setAccessible(true);
        List<Result> value = new ArrayList<>();
        field.set(consulta, value);


        //when
        final List<Result> result = consulta.getResults();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setResultsTest() throws NoSuchFieldException, IllegalAccessException {
        List<Result>value = new ArrayList<>();

        consulta.setResults(value);
        final Field field = consulta.getClass().getDeclaredField("results");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(consulta), value);
    }

    @Test
    public void getSeoTitleTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = consulta.getClass().getDeclaredField("seoTitle");
        field.setAccessible(true);
        String value = "texto";
        field.set(consulta, value);


        //when
        final String result = consulta.getSeoTitle();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setSeoTitleTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        consulta.setSeoTitle(value);
        final Field field = consulta.getClass().getDeclaredField("seoTitle");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(consulta), value);
    }

    @Test
    public void getSeoDescriptionTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = consulta.getClass().getDeclaredField("seoDescription");
        field.setAccessible(true);
        String value = "texto";
        field.set(consulta, value);


        //when
        final String result = consulta.getSeoDescription();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setSeoDescriptionTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        consulta.setDescription(value);
        final Field field = consulta.getClass().getDeclaredField("description");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(consulta), value);
    }

    @Test
    public void getSeoKeywordsTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = consulta.getClass().getDeclaredField("seoKeywords");
        field.setAccessible(true);
        String value = "texto";
        field.set(consulta, value);


        //when
        final String result = consulta.getSeoKeywords();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setSeoKeywordsTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        consulta.setSeoKeywords(value);
        final Field field = consulta.getClass().getDeclaredField("seoKeywords");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(consulta), value);
    }

    @Test
    public void getSeoH1Test() throws NoSuchFieldException, IllegalAccessException {
        final Field field = consulta.getClass().getDeclaredField("seoH1");
        field.setAccessible(true);
        String value = "texto";
        field.set(consulta, value);


        //when
        final String result = consulta.getSeoH1();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setSeoH1Test() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        consulta.setSeoH1(value);
        final Field field = consulta.getClass().getDeclaredField("seoH1");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(consulta), value);
    }

    @Test
    public void getNoindexTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = consulta.getClass().getDeclaredField("noindex");
        field.setAccessible(true);
        boolean value = true;
        field.set(consulta, value);


        //when
        final boolean result = consulta.getNoindex();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setNoindexTest() throws NoSuchFieldException, IllegalAccessException {
        boolean value = true;

        consulta.setNoindex(value);
        final Field field = consulta.getClass().getDeclaredField("noindex");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(consulta), value);
    }

    @Test
    public void getNofollowTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = consulta.getClass().getDeclaredField("nofollow");
        field.setAccessible(true);
        boolean value = true;
        field.set(consulta, value);


        //when
        final boolean result = consulta.getNofollow();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setNofollowTest() throws NoSuchFieldException, IllegalAccessException {
        boolean value = true;

        consulta.setNofollow(value);
        final Field field = consulta.getClass().getDeclaredField("nofollow");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(consulta), value);
    }

    @Test
    public void getDescriptionTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = consulta.getClass().getDeclaredField("description");
        field.setAccessible(true);
        String value = "texto";
        field.set(consulta, value);


        //when
        final String result = consulta.getDescription();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setDescriptionTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        consulta.setDescription(value);
        final Field field = consulta.getClass().getDeclaredField("description");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(consulta), value);
    }

    @Test
    public void getFiltersTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = consulta.getClass().getDeclaredField("filters");
        field.setAccessible(true);
        Filters value = new Filters();
        field.set(consulta, value);


        //when
        final Filters result = consulta.getFilters();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setFiltersTest() throws NoSuchFieldException, IllegalAccessException {
        Filters value = new Filters();

        consulta.setFilters(value);
        final Field field = consulta.getClass().getDeclaredField("filters");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(consulta), value);
    }

    @Test
    public void getNofollowCollectionsTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = consulta.getClass().getDeclaredField("nofollowCollections");
        field.setAccessible(true);
        List<String> value = new ArrayList<>();
        field.set(consulta, value);


        //when
        final List<String> result = consulta.getNofollowCollections();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setNofollowCollectionsTest() throws NoSuchFieldException, IllegalAccessException {
        List<String> value = new ArrayList<>();

        consulta.setNofollowCollections(value);
        final Field field = consulta.getClass().getDeclaredField("nofollowCollections");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(consulta), value);
    }
}