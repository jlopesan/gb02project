package es.unex.giiis.gb02project.entities;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.dataApp.model.Tag;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersWishlist;

import static org.junit.Assert.*;

public class UsersWishlistTest {

    private UsersWishlist instance;

    @Before
    public void setUp() throws Exception{
        instance = new UsersWishlist(1, 2, "NameExample", "BackgroundImageExample", 3.0, "DescriptionExample", "BackgroundImageAdditionalExample");
    }

    @Test
    public void getIDUserTest() throws NoSuchFieldException, IllegalAccessException  {
        final Field field = instance.getClass().getDeclaredField("mIDUser");
        field.setAccessible(true);
        field.set(instance, 1);
        //when
        final long result = instance.getIDUser();
        //then
        assertEquals("field wasn't retrieved properly", result, 1);
    }

    @Test
    public void getIDGameTest() throws NoSuchFieldException, IllegalAccessException  {
        final Field field = instance.getClass().getDeclaredField("mIDGame");
        field.setAccessible(true);
        field.set(instance, 1);
        //when
        final Integer result = instance.getIDGame();
        //then
        assertEquals("field wasn't retrieved properly", result.intValue(), 1);
    }

    @Test
    public void getNameTest() throws NoSuchFieldException, IllegalAccessException  {
        final Field field = instance.getClass().getDeclaredField("mName");
        field.setAccessible(true);
        field.set(instance, "NameExample");
        //when
        final String result = instance.getName();
        //then
        assertEquals("field wasn't retrieved properly", result, "NameExample");
    }

    @Test
    public void getBackgroundImageTest() throws NoSuchFieldException, IllegalAccessException  {
        final Field field = instance.getClass().getDeclaredField("mBackgroundImage");
        field.setAccessible(true);
        field.set(instance, "BackgroundImageExample");
        //when
        final String result = instance.getBackgroundImage();
        //then
        assertEquals("field wasn't retrieved properly", result, "BackgroundImageExample");
    }

    @Test
    public void getRatingTest() throws NoSuchFieldException, IllegalAccessException  {
        final Field field = instance.getClass().getDeclaredField("mRating");
        field.setAccessible(true);
        field.set(instance, 2.25);
        //when
        final Double result = instance.getRating();
        //then
        assertEquals("field wasn't retrieved properly", result.doubleValue(), 2.25, 0.0001);
    }

    @Test
    public void getDescriptionTest() throws NoSuchFieldException, IllegalAccessException  {
        final Field field = instance.getClass().getDeclaredField("mDescription");
        field.setAccessible(true);
        field.set(instance, "DescriptionExample");
        //when
        final String result = instance.getDescription();
        //then
        assertEquals("field wasn't retrieved properly", result, "DescriptionExample");
    }

    @Test
    public void getBackground_image_additionalTest() throws NoSuchFieldException, IllegalAccessException  {
        final Field field = instance.getClass().getDeclaredField("mBackground_image_additional");
        field.setAccessible(true);
        field.set(instance, "BackgroundImageAdditionalExample");
        //when
        final String result = instance.getBackground_image_additional();
        //then
        assertEquals("field wasn't retrieved properly", result, "BackgroundImageAdditionalExample");
    }

    @Test
    public void setIDUserTest() throws NoSuchFieldException, IllegalAccessException  {
        Long value = new Long(1);
        instance.setIDUser(value);
        final Field field = instance.getClass().getDeclaredField("mIDUser");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setIDGameTest() throws NoSuchFieldException, IllegalAccessException  {
        Integer value = 1;
        instance.setIDGame(value);
        final Field field = instance.getClass().getDeclaredField("mIDGame");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setNameTest() throws NoSuchFieldException, IllegalAccessException  {
        String value = "NameExample";
        instance.setName(value);
        final Field field = instance.getClass().getDeclaredField("mName");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setBackgroundImageTest() throws NoSuchFieldException, IllegalAccessException  {
        String value = "BackgroundImageExample";
        instance.setBackgroundImage(value);
        final Field field = instance.getClass().getDeclaredField("mBackgroundImage");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setRatingTest() throws NoSuchFieldException, IllegalAccessException  {
        Double value = 2.0;
        instance.setRating(value);
        final Field field = instance.getClass().getDeclaredField("mRating");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setDescriptionTest() throws NoSuchFieldException, IllegalAccessException  {
        String value = "DescriptionExample";
        instance.setDescription(value);
        final Field field = instance.getClass().getDeclaredField("mDescription");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setBackground_image_additionalTest() throws NoSuchFieldException, IllegalAccessException  {
        String value = "BackgroundImageAdditionalExample";
        instance.setBackground_image_additional(value);
        final Field field = instance.getClass().getDeclaredField("mBackground_image_additional");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void toResultTest() throws NoSuchFieldException, IllegalAccessException  {
        final Field field1 = instance.getClass().getDeclaredField("mIDUser");
        field1.setAccessible(true);
        field1.set(instance, 1);

        final Field field2 = instance.getClass().getDeclaredField("mIDGame");
        field2.setAccessible(true);
        field2.set(instance, 2);

        final Field field3 = instance.getClass().getDeclaredField("mName");
        field3.setAccessible(true);
        field3.set(instance, "NameToResultExample");

        final Field field4 = instance.getClass().getDeclaredField("mBackgroundImage");
        field4.setAccessible(true);
        field4.set(instance, "BackgroundImageToResultExample");

        final Field field5 = instance.getClass().getDeclaredField("mRating");
        field5.setAccessible(true);
        field5.set(instance, 3.1);

        final Field field6 = instance.getClass().getDeclaredField("mDescription");
        field6.setAccessible(true);
        field6.set(instance, "DescriptionToResultExample");

        final Field field7 = instance.getClass().getDeclaredField("mBackground_image_additional");
        field7.setAccessible(true);
        field7.set(instance, "BackgroundImageAdditionalToResultExample");


        Result result = instance.toResult();

        assertEquals("field wasn't retrieved properly", result.getId(), new Integer(2));
        assertEquals("field wasn't retrieved properly", result.getName(), "NameToResultExample");
        assertEquals("field wasn't retrieved properly", result.getBackgroundImage(), "BackgroundImageToResultExample");
        assertEquals("field wasn't retrieved properly", result.getBackgroundImageAdditional(), "BackgroundImageAdditionalToResultExample");
        assertEquals("field wasn't retrieved properly", result.getDescription(), "DescriptionToResultExample");
        assertEquals("field wasn't retrieved properly", result.getRating(), new Double(3.1));
    }

}