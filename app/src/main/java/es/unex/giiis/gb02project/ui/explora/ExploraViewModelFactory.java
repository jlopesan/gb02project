package es.unex.giiis.gb02project.ui.explora;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.gb02project.dataApp.ExploraRepository;


public class ExploraViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final ExploraRepository mRepository;

    public ExploraViewModelFactory(ExploraRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new ExploraViewModel(mRepository);
    }
}