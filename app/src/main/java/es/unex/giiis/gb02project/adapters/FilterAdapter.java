package es.unex.giiis.gb02project.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.recyclerview.widget.RecyclerView;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.gb02project.R;
import es.unex.giiis.gb02project.dataApp.model.Consulta;
import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Genre;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PlatformCategory;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.MyViewHolder> {
    private List<Pair<Integer, String>> mDataset;
    private Context mContext;
    private List<Integer> genreList;
    private String mlistName;
    private Intent mIntent;

    public interface OnListInteractionListener{
        public void onListInteraction(String url);
    }

    public OnListInteractionListener mListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public Button filterOptionButton;
        public View mView;

        public Pair<Integer, String> mItem;




        public MyViewHolder(View v) {
            super(v);
            mView=v;
            filterOptionButton = v.findViewById(R.id.filter_option_button);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public FilterAdapter(String listName, List<Pair<Integer, String>> myDataset, OnListInteractionListener listener, Context context, Intent intent){
        mDataset = myDataset;
        mListener = listener;
        mContext = context;
        mIntent = intent;
        genreList = new ArrayList<>();
        mlistName = listName;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        // Create new views (invoked by the layout manager)
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.filter_item_view, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.mItem = mDataset.get(position);
        holder.filterOptionButton.setText(mDataset.get(position).second);
        Drawable background = holder.filterOptionButton.getBackground();
        holder.filterOptionButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    //Intent intent = new Intent(mContext, SearchActivity.class);
                    int i = genreList.indexOf(mDataset.get(position).first);
                    if (i != -1){
                        genreList.remove(i);
                        holder.filterOptionButton.setBackground(background);
                        //holder.filterOptionButton.setText("*");
                    }
                    else{
                        genreList.add(mDataset.get(position).first);
                        holder.filterOptionButton.setBackgroundColor(Color.parseColor("#82D86A"));
                        //holder.filterOptionButton.setText("*");
                    }
                    mIntent.putExtra(mlistName, (Serializable) genreList);

                    //mListener.onListInteraction(holder.mItem.getBackgroundImage());
                }
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

/*
    public void swap(Consulta dataset){
        List<Pair<Integer, String>> aux = new ArrayList<>();
        for(Result r: dataset.getResults()){
            aux.add(new Pair(r.getId(), r.getName()));
        }
        mDataset = aux;
        notifyDataSetChanged();
    }

 */

    public void swapPlatform(List<PlatformCategory> dataset){
        List<Pair<Integer, String>> aux = new ArrayList<>();
        for(PlatformCategory r: dataset){
            aux.add(new Pair(r.getIDPlatform(), r.getName()));
        }
        mDataset = aux;
        notifyDataSetChanged();
    }


    public void swapGenre(List<Genre> dataset){
        List<Pair<Integer, String>> aux = new ArrayList<>();
        for(Genre r: dataset){
            aux.add(new Pair(r.getIDGenre(), r.getName()));
        }
        mDataset = aux;
        notifyDataSetChanged();
    }



    public void deleteData() {
        mDataset.clear();
        notifyItemRangeRemoved(0, getItemCount());
    }

}
