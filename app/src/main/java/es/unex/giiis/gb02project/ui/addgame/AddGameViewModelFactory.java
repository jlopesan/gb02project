package es.unex.giiis.gb02project.ui.addgame;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.gb02project.dataApp.PerfilRepository;


public class AddGameViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final PerfilRepository mRepository;

    public AddGameViewModelFactory(PerfilRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new AddGameViewModel(mRepository);
    }
}