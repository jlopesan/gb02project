package es.unex.giiis.gb02project.ui.signin;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import es.unex.giiis.gb02project.dataApp.PerfilRepository;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.User;

public class SigninViewModel extends ViewModel {

    private final PerfilRepository mRepository;

    public SigninViewModel(PerfilRepository perfilRepository) {
        mRepository = perfilRepository;
    }

    public User getUserByUsername(String username){
        return (mRepository.getUserByUsername(username));
    }

    public LiveData<User> getUserByUsernameLiveData(String username){
        return (mRepository.getUserByUsernameLiveData(username));
    }

    public void insert(User user){
        mRepository.insertUser(user);
    }
}
