package es.unex.giiis.gb02project.ui.editAccount;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.PerfilRepository;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.User;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersPlaying;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersWishlist;

public class EditAccountViewModel extends ViewModel {

    private final PerfilRepository mRepository;

    public EditAccountViewModel(PerfilRepository perfilRepository) {
        mRepository = perfilRepository;
    }

    public void updateUser(User user){
        mRepository.updateUser(user);
    }


    public void insert(User user){
        mRepository.insertUser(user);
    }

    public LiveData<User> getUserByUsernameLiveData(String username){
        return (mRepository.getUserByUsernameLiveData(username));
    }

}