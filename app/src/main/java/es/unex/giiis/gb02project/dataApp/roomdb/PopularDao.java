package es.unex.giiis.gb02project.dataApp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.PopularGames;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface PopularDao {

    @Insert(onConflict = REPLACE)
    void bulkInsert(List<PopularGames> popularGames);

    @Query("SELECT * FROM popular_games")
    public LiveData<List<PopularGames>> getAll();

    @Insert
    public long insert(PopularGames popularGame);

    @Query("DELETE FROM popular_games")
    public void deleteAll();

    @Query("SELECT * FROM popular_games WHERE id = :id")
    public PopularGames getByID(Integer id);

    @Query("SELECT count(id) FROM popular_games")
    public Integer getNumberOfPopularGames();

}
