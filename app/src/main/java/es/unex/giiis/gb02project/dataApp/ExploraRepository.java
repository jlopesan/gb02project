package es.unex.giiis.gb02project.dataApp;

import android.util.Log;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.unex.giiis.gb02project.AppExecutors;
import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.dataApp.network.ConsultaNetworkDataSource;
import es.unex.giiis.gb02project.dataApp.network.ConsultaNetworkLoaderRunnable;
import es.unex.giiis.gb02project.dataApp.network.ResultNetworkDataSource;
import es.unex.giiis.gb02project.dataApp.roomdb.GenreDao;
import es.unex.giiis.gb02project.dataApp.roomdb.ImagesDao;
import es.unex.giiis.gb02project.dataApp.roomdb.NewDao;
import es.unex.giiis.gb02project.dataApp.roomdb.PlatformCategoryDao;
import es.unex.giiis.gb02project.dataApp.roomdb.PlatformDao;
import es.unex.giiis.gb02project.dataApp.roomdb.PopularDao;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Genre;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.ImagesGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.NewGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PlatformCategory;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PopularGames;


public class ExploraRepository {
    private static final String LOG_TAG = ExploraRepository.class.getSimpleName();

    // For Singleton instantiation
    private static ExploraRepository sInstance;
    private final PopularDao mPopularDao;
    private final NewDao mNewDao;
    private final ImagesDao mImageDao;
    private final PlatformDao mPlatformDao;
    private final PlatformCategoryDao mPlatformCategoryDao;
    private final GenreDao mGenreDao;
    private final ResultNetworkDataSource mResultNetworkDataSource;
    private final ConsultaNetworkDataSource mConsultaNetworkDataSource;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private final LiveData<List<Result>> mNetworkSearchGamesData;
    private final MutableLiveData<Integer> userFilterLiveData = new MutableLiveData<>(); //ATENCION - mutableLiveData porque se le puede poner el valor que yo quiera
    //private final Map<String, Long> lastUpdateTimeMillisMap = new HashMap<>();
    private long lastUpdateTimeMillisPopular;
    private long lastUpdateTimeMillisNew;
    private long lastUpdateTimeMillisPlatforms;
    private long lastUpdateTimeMillisGenres;
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS = 20000;
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS_FILTER = 20000;
    private final String values = new String("New values inserted in Room");

    private ExploraRepository(ImagesDao imageDao, PopularDao popularDao, NewDao newDao, PlatformDao platformDao, PlatformCategoryDao platformCategoryDao, GenreDao genreDao, ResultNetworkDataSource resultNetworkDataSource, ConsultaNetworkDataSource consultaNetworkDataSource) {
        mPopularDao = popularDao;
        mNewDao = newDao;
        mImageDao= imageDao;
        mPlatformDao = platformDao;
        mPlatformCategoryDao = platformCategoryDao;
        mGenreDao = genreDao;
        mResultNetworkDataSource = resultNetworkDataSource;
        mConsultaNetworkDataSource = consultaNetworkDataSource;
        mNetworkSearchGamesData = mResultNetworkDataSource.getCurrentSearchGames();

        Map<String, String> mPopular = new HashMap<>();
        doFetchPopular(mPopular, null);

        doFetchPlatforms();
        doFetchGenres();

        // LiveData that fetches repos from network
        LiveData<PopularGames[]> networkData = mResultNetworkDataSource.getCurrentPopularGames();
        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkData.observeForever(newPopularFromNetwork -> {
            mExecutors.diskIO().execute(() -> {
                // Deleting cached repos of user
                if (newPopularFromNetwork.length > 0){
                    mPopularDao.deleteAll();
                }
                // Insert our new repos into local database
                mPopularDao.bulkInsert(Arrays.asList(newPopularFromNetwork));
                Log.d(LOG_TAG, values);
            });
        });

        // LiveData that fetches repos from network
        LiveData<NewGames[]> networkNewGamesData = mResultNetworkDataSource.getCurrentNewGames();
        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkNewGamesData.observeForever(new Observer<NewGames[]>() {
            @Override
            public void onChanged(NewGames[] newNewGamesFromNetwork) {
                mExecutors.diskIO().execute(() -> {
                    // Deleting cached repos of user
                    if (newNewGamesFromNetwork.length > 0) {
                        mNewDao.deleteAll();
                    }
                    // Insert our new repos into local database
                    mNewDao.bulkInsert(Arrays.asList(newNewGamesFromNetwork));
                    Log.d(LOG_TAG, values);
                });
            }
        });


        LiveData<List<PlatformCategory>> networkPlatformCategoriesData = mConsultaNetworkDataSource.getCurrentPlatformCategories();
        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkPlatformCategoriesData.observeForever(new Observer<List<PlatformCategory>>() {
            @Override
            public void onChanged(List<PlatformCategory> newPlatformCategoriesFromNetwork) {
                mExecutors.diskIO().execute(() -> {
                    // Deleting cached repos of user
                    if (newPlatformCategoriesFromNetwork.size() > 0) {
                        mPlatformCategoryDao.deleteAll();
                    }
                    // Insert our new repos into local database
                    mPlatformCategoryDao.bulkInsert(newPlatformCategoriesFromNetwork);
                    Log.d(LOG_TAG, values);
                });
            }
        });

        LiveData<List<Genre>> networkGenresData = mConsultaNetworkDataSource.getCurrentGenres();
        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkGenresData.observeForever(new Observer<List<Genre>>() {
            @Override
            public void onChanged(List<Genre> newGenresFromNetwork) {
                mExecutors.diskIO().execute(() -> {
                    // Deleting cached repos of user
                    if (newGenresFromNetwork.size() > 0) {
                        mGenreDao.deleteAll();
                    }
                    // Insert our new repos into local database
                    mGenreDao.bulkInsert(newGenresFromNetwork);
                    Log.d(LOG_TAG, values);
                });
            }
        });


        // LiveData that fetches repos from network
        LiveData<List<Platform>> networkPlatformData = mResultNetworkDataSource.getCurrentPlatforms();
        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkPlatformData.observeForever(new Observer<List<Platform>>() {
            @Override
            public void onChanged(List<Platform> newPlatformFromNetwork) {
                mExecutors.diskIO().execute(() -> {
                    // Deleting cached repos of user
                    if (newPlatformFromNetwork.size() > 0) {
                        mPlatformDao.deleteType(Platform.TipoPopular);
                        mPlatformDao.deleteType(Platform.TipoNovedad);
                    }
                    // Insert our new repos into local database
                    mPlatformDao.bulkInsert(newPlatformFromNetwork);
                    Log.d(LOG_TAG, values);
                });
            }
        });

        // LiveData that fetches repos from network
        LiveData<List<ImagesGame>> networkImagesData = mResultNetworkDataSource.getCurrentImages();
        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkImagesData.observeForever(new Observer<List<ImagesGame>>() {
            @Override
            public void onChanged(List<ImagesGame> newImageFromNetwork) {
                mExecutors.diskIO().execute(() -> {
                    // Deleting cached repos of user
                    if (newImageFromNetwork.size() > 0) {
                        mImageDao.deleteAll();
                    }
                    // Insert our new repos into local database
                    mImageDao.bulkInsert(newImageFromNetwork);
                    Log.d(LOG_TAG, values);
                });
            }
        });
    }

    public synchronized static ExploraRepository getInstance(ImagesDao imagesDao,PopularDao populardao, NewDao newDao, PlatformDao platformDao, PlatformCategoryDao platformCategoryDao, GenreDao genreDao, ResultNetworkDataSource nds, ConsultaNetworkDataSource cnds) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new ExploraRepository(imagesDao,populardao, newDao, platformDao, platformCategoryDao, genreDao, nds, cnds);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }
/*
    public void setUsername(final String username){
        // TO-DO - Set value to MutableLiveData in order to filter getCurrentRepos LiveData
        userFilterLiveData.setValue(username);

      /*  AppExecutors.getInstance().diskIO().execute(() -> {
            if (isFetchNeeded(username)) {
                doFetchResult(username);
            }
        });


    }
*/
    public void getSearchGames (Map<String, String> params){
        AppExecutors.getInstance().diskIO().execute(() -> mResultNetworkDataSource.fetchSearchGames(params));
    }

    public void getPopularGames(Map<String, String> mPopular){
        // TO-DO - Set value to MutableLiveData in order to filter getCurrentRepos LiveData

       AppExecutors.getInstance().diskIO().execute(() -> {
            if (isFetchNeededPopular()) {
                doFetchPopular(mPopular, null);
            }
        });

    }

    public void getPlatforms(){
        // TO-DO - Set value to MutableLiveData in order to filter getCurrentRepos LiveData
        AppExecutors.getInstance().diskIO().execute(() -> {
            if (isFetchNeededPlatform()) {
                doFetchPlatforms();
            }
        });
    }

    public void getGenres(){
        // TO-DO - Set value to MutableLiveData in order to filter getCurrentRepos LiveData
        AppExecutors.getInstance().diskIO().execute(() -> {
            if (isFetchNeededGenre()) {
                doFetchGenres();
            }
        });
    }


    public void getNewGames(Map<String, String> mNew){
        // TO-DO - Set value to MutableLiveData in order to filter getCurrentRepos LiveData

        AppExecutors.getInstance().diskIO().execute(() -> {
            if (isFetchNeededNew()) {
                doFetchNew(mNew, null);
            }
        });

    }
    public void doFetchPopular(Map<String, String> parametros, List<Long> completados){
        Log.d(LOG_TAG, "Fetching Repos from Github");
        AppExecutors.getInstance().diskIO().execute(() -> {
            mPopularDao.deleteAll();
            mResultNetworkDataSource.fetchPopular(parametros, completados);
            //lastUpdateTimeMillisMap.put(System.currentTimeMillis());
            lastUpdateTimeMillisPopular = System.currentTimeMillis();
        });
    }

    public void doFetchNew(Map<String, String> parametros, List<Long> completados){
        Log.d(LOG_TAG, "Fetching Repos from Github");
        AppExecutors.getInstance().diskIO().execute(() -> {
            mNewDao.deleteAll();
            mResultNetworkDataSource.fetchNew(parametros, completados);
            //lastUpdateTimeMillisMap.put(System.currentTimeMillis());
            lastUpdateTimeMillisNew = System.currentTimeMillis();
        });
    }

    public void doFetchPlatforms(){
        Log.d(LOG_TAG, "Fetching platforms from Github");
        AppExecutors.getInstance().diskIO().execute(() -> {
            mPlatformCategoryDao.deleteAll();
            mConsultaNetworkDataSource.fetchPlatformCategories();
            lastUpdateTimeMillisPlatforms = System.currentTimeMillis();
        });
    }

    public void doFetchGenres(){
        Log.d(LOG_TAG, "Fetching genres from Github");
        AppExecutors.getInstance().diskIO().execute(() -> {
            mGenreDao.deleteAll();
            mConsultaNetworkDataSource.fetchGenres();
            lastUpdateTimeMillisGenres = System.currentTimeMillis();
        });
    }

    /**
     * Database related operations
     **/

    public LiveData<List<PopularGames>> getCurrentPopular() {
        return mPopularDao.getAll();
    }

    public void setGame(final Integer idgame){
        userFilterLiveData.setValue(idgame);
        //getCurrentUsersCompleted();
    }


    public LiveData<List<NewGames>> getCurrentNew() {
        return mNewDao.getAll();
    }


    public LiveData<List<Platform>> getCurrentPlatforms(){
        return Transformations.switchMap(userFilterLiveData, new Function<Integer, LiveData<List<Platform>>>() {
            @Override
            public LiveData<List<Platform>> apply(Integer user) {
                return mPlatformDao.getLiveData(user);
            }
        });
    }

    public LiveData<List<PlatformCategory>> getCurrentPlatformCategories(){
        return mConsultaNetworkDataSource.getCurrentPlatformCategories();
    }

    public LiveData<List<Genre>> getCurrentGenres(){
        return mConsultaNetworkDataSource.getCurrentGenres();
    }

    public LiveData<List<Result>> getCurrentSearchGames(){
        return mNetworkSearchGamesData;
    }


    private boolean isFetchNeededPopular() {
        Long lastFetchTimeMillis = lastUpdateTimeMillisPopular;
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;
        // TO-DO - Implement cache policy: When time has passed or no repos in cache
        return timeFromLastFetch > MIN_TIME_FROM_LAST_FETCH_MILLIS || mPopularDao.getNumberOfPopularGames() == 0;
    }

    private boolean isFetchNeededNew() {
        Long lastFetchTimeMillis = lastUpdateTimeMillisNew;
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;
        // TO-DO - Implement cache policy: When time has passed or no repos in cache
        return timeFromLastFetch > MIN_TIME_FROM_LAST_FETCH_MILLIS || mNewDao.getNumberOfNewGames() == 0;
    }

    private boolean isFetchNeededPlatform() {
        Long lastFetchTimeMillis = lastUpdateTimeMillisPlatforms;
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;
        // TO-DO - Implement cache policy: When time has passed or no repos in cache
        return timeFromLastFetch > MIN_TIME_FROM_LAST_FETCH_MILLIS_FILTER || mPlatformCategoryDao.getAll().size() == 0;
    }

    private boolean isFetchNeededGenre() {
        Long lastFetchTimeMillis = lastUpdateTimeMillisGenres;
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;
        // TO-DO - Implement cache policy: When time has passed or no repos in cache
        return timeFromLastFetch > MIN_TIME_FROM_LAST_FETCH_MILLIS_FILTER || mGenreDao.getAll().size() == 0;
    }

}