package es.unex.giiis.gb02project.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import es.unex.giiis.gb02project.R;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;

public class CustomGamesAdapter extends RecyclerView.Adapter<CustomGamesAdapter.MyViewHolder> {


    private List<CustomGame> mDataset;
    private Context mContext;



    public interface OnListInteractionListener{
        public void onListInteraction(String url);
    }

    public OnListInteractionListener mListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public TextView mDateView;
        public ImageView mImageView;
        public View mView;

        public CustomGame mItem;

        public MyViewHolder(View v) {
            super(v);
            mView=v;
            mTextView = v.findViewById(R.id.textView);
            mImageView = v.findViewById(R.id.imageGame);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CustomGamesAdapter(List<CustomGame> myDataset, OnListInteractionListener listener, Context context){
        mDataset = myDataset;
        mListener = listener;
        mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        // Create new views (invoked by the layout manager)
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_text_view, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.mItem = mDataset.get(position);
        holder.mTextView.setText(mDataset.get(position).getGamename());

        RequestOptions requestOptions = new RequestOptions().centerCrop();
        Glide.with(mContext)
                .load("https://graffica.info/wp-content/uploads/2019/03/stadiavideojueos-1024x659.jpg")
                .apply(requestOptions)
                .placeholder(R.drawable.ic_notifications_black_24dp)
                .into(holder.mImageView);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }



    public void swap(List<CustomGame> dataset){
        mDataset = dataset;
        notifyDataSetChanged();
    }
}
