package es.unex.giiis.gb02project.dataApp.roomdb.entities;


import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;

import java.io.Serializable;

import es.unex.giiis.gb02project.dataApp.model.Result;

@Entity(tableName = "users_completed", primaryKeys = {"IDUser", "IDGame"})
public class UsersCompleted {
    @Ignore
    public static final String ITEM_SEP = System.getProperty("line.separator");

    @Ignore
    public final static String IDUser = "IDUser";
    @Ignore
    public final static String IDGame = "IDGame";
    @Ignore
    public final static String NAME = "name";
    @Ignore
    public final static String BACKGROUNDIMAGE = "backgroundImage";
    @Ignore
    public final static String RATING = "rating";
    @Ignore
    public final static String DESCRIPTION = "description";
    @Ignore
    public final static String BACKGROUND_IMAGE_ADDITIONAL = "background_image_additional";

    @ColumnInfo(name = "IDUser") @NonNull
    private long mIDUser;

    @ColumnInfo(name = "IDGame") @NonNull
    private Integer mIDGame;

    @ColumnInfo(name = "name")
    private String mName = new String();

    @ColumnInfo(name = "backgroundImage")
    private String mBackgroundImage = new String();

    @ColumnInfo(name = "rating")
    private Double mRating;

    @ColumnInfo(name ="description")
    private String mDescription = new String();

    @ColumnInfo(name ="background_image_additional")
    private String mBackground_image_additional = new String();


    public UsersCompleted(long mIDUser, Integer mIDGame, String mName, String mBackgroundImage, Double mRating, String mDescription, String mBackground_image_additional) {
        this.mIDUser = mIDUser;
        this.mIDGame = mIDGame;
        this.mName = mName;
        this.mBackgroundImage = mBackgroundImage;
        this.mRating = mRating;
        this.mDescription = mDescription;
        this.mBackground_image_additional = mBackground_image_additional;
    }


    // Create a new User from data packaged in an Intent

    @Ignore
    public UsersCompleted(Intent intent) {
        mIDUser = intent.getLongExtra(UsersCompleted.IDUser,0);
        mIDGame = intent.getIntExtra(UsersCompleted.IDGame, 0);
        mName = intent.getStringExtra(UsersCompleted.NAME);
        mBackgroundImage = intent.getStringExtra(UsersCompleted.BACKGROUNDIMAGE);
        mRating = intent.getDoubleExtra(UsersCompleted.RATING, 0);
        mDescription = intent.getStringExtra(UsersCompleted.DESCRIPTION);
        mBackground_image_additional = intent.getStringExtra(UsersCompleted.BACKGROUND_IMAGE_ADDITIONAL);

    }

    public long getIDUser() { return mIDUser; }

    public Integer getIDGame() { return mIDGame; }

    public String getName() {
        return mName;
    }

    public String getBackgroundImage() {
        return mBackgroundImage;
    }

    public Double getRating() {
        return mRating;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getBackground_image_additional() {
        return mBackground_image_additional;
    }

    public void setIDUser(long IDUser) { this.mIDUser = IDUser; }

    public void setIDGame(Integer IDGame) { this.mIDGame = IDGame; }

    public void setName(String mName) {
        this.mName = mName;
    }

    public void setBackgroundImage(String mBackgroundImage) {
        this.mBackgroundImage = mBackgroundImage;
    }

    public void setRating(Double mRating) {
        this.mRating = mRating;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public void setBackground_image_additional(String mBackground_image_additional) {
        this.mBackground_image_additional = mBackground_image_additional;
    }


    // Take a set of String data values and
    // package them for transport in an Intent

    public static void packageIntent(Intent intent, long IDUser, Integer IDGame, String mName, String mBackgroundImage, Double mRating, String mDescription, String mBackground_image_additional) {
        intent.putExtra(UsersCompleted.IDUser, IDUser);
        intent.putExtra(UsersCompleted.IDGame, IDGame);
        intent.putExtra(UsersCompleted.NAME, mName);
        intent.putExtra(UsersCompleted.BACKGROUNDIMAGE, mBackgroundImage);
        intent.putExtra(UsersCompleted.RATING, mRating);
        intent.putExtra(UsersCompleted.DESCRIPTION, mDescription);
        intent.putExtra(UsersCompleted.BACKGROUND_IMAGE_ADDITIONAL, mBackground_image_additional);

    }

    @Ignore
    public Result toResult(){
        Result result = new Result();
        result.setName(mName);
        result.setId(mIDGame);
        result.setBackgroundImage(mBackgroundImage);
        result.setBackgroundImageAdditional(mBackground_image_additional);
        result.setDescription(mDescription);
        result.setRating(mRating);

        return result;
    }

}
