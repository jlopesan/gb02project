package es.unex.giiis.gb02project.dataApp.network;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;

import es.unex.giiis.gb02project.AppExecutors;
import es.unex.giiis.gb02project.dataApp.model.*;

public class ConsultaLoaderRunnable implements Runnable {

    private final InputStream mInFile;
    private final OnConsultaLoadedListener mOnConsultaLoadedListener;

    public ConsultaLoaderRunnable(InputStream inFile, OnConsultaLoadedListener onReposLoadedListener) {
        mInFile = inFile;
        mOnConsultaLoadedListener = onReposLoadedListener;
    }

    @Override
    public void run() {
        // Obtención de los datos a partir del InputStream

        // Parse json file into JsonReader
        JsonReader reader = new JsonReader(new InputStreamReader(mInFile));
        // Parse JsonReader into list of Repo using Gson
        Consulta consulta = new Gson().fromJson(reader, Consulta.class);

        // Llamada al Listener con los datos obtenidos
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
               mOnConsultaLoadedListener.onConsultaLoaded(consulta);
            }
        });


    }
}