
package es.unex.giiis.gb02project.dataApp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Platform implements Serializable {

    @SerializedName("platform")
    @Expose
    private Platform_ platform;
    @SerializedName("released_at")
    @Expose
    private String releasedAt;
    @SerializedName("requirements_en")
    @Expose
    private Object requirementsEn;
    @SerializedName("requirements_ru")
    @Expose
    private Object requirementsRu;

    public Platform_ getPlatform() {
        return platform;
    }

    public void setPlatform(Platform_ platform) {
        this.platform = platform;
    }

    public String getReleasedAt() {
        return releasedAt;
    }

    public void setReleasedAt(String releasedAt) {
        this.releasedAt = releasedAt;
    }

    public Object getRequirementsEn() {
        return requirementsEn;
    }

    public void setRequirementsEn(Object requirementsEn) {
        this.requirementsEn = requirementsEn;
    }

    public Object getRequirementsRu() {
        return requirementsRu;
    }

    public void setRequirementsRu(Object requirementsRu) {
        this.requirementsRu = requirementsRu;
    }

}
