
package es.unex.giiis.gb02project.dataApp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ParentPlatform implements Serializable {

    @SerializedName("platform")
    @Expose
    private Platform__ platform;

    public Platform__ getPlatform() {
        return platform;
    }

    public void setPlatform(Platform__ platform) {
        this.platform = platform;
    }

}
