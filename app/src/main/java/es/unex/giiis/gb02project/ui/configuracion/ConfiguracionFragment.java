package es.unex.giiis.gb02project.ui.configuracion;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import es.unex.giiis.gb02project.AppContainer;
import es.unex.giiis.gb02project.MyApplication;
import es.unex.giiis.gb02project.ui.editAccount.EditAccountActivity;
import es.unex.giiis.gb02project.R;
import es.unex.giiis.gb02project.ui.login.LoginActivity;
import es.unex.giiis.gb02project.ui.signin.SigninActivity;

public class ConfiguracionFragment extends Fragment {

    private ConfiguracionViewModel configuracionViewModel;
    private final static int USER_LOGGED = 0;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        configuracionViewModel = new ViewModelProvider(this, appContainer.configuracionViewModelFactory).get(ConfiguracionViewModel.class);

        View root = inflater.inflate(R.layout.fragment_configuracion, container, false);
        //final TextView textView = root.findViewById(R.id.text_configuracion);
        final Button bEditarCuenta = root.findViewById(R.id.editar_cuenta);



        //SharedPreferences sp = ("MyPref", Context.MODE_PRIVATE);

        bEditarCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EditAccountActivity.class);
                startActivity(intent);
            }
        });
        /*

        configuracionViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

         */
        final Context context = this.getContext();
        final Button bdelAccount = root.findViewById(R.id.delAccount);
        final SharedPreferences sp = getActivity().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        long logged = sp.getLong("UserID", -1);

        final Button bIniSes = root.findViewById(R.id.ini_ses_buttom);
        final Button bReg = root.findViewById(R.id.regAccount);
        final Button bAbout = root.findViewById(R.id.about_us_buttom);
        final Button bfaq = root.findViewById(R.id.faq_buttom);
        if(logged == -1){
            bdelAccount.setVisibility(View.INVISIBLE);
            bEditarCuenta.setVisibility(View.INVISIBLE);
        }else{
            bIniSes.setVisibility(View.INVISIBLE);
            bReg.setVisibility(View.INVISIBLE);
        }

        bIniSes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivityForResult(intent, USER_LOGGED);
            }
        });


        bReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SigninActivity.class);
                startActivityForResult(intent, USER_LOGGED);
            }
        });

        bAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Sobre nosotros");
                builder.setMessage("¡Con esta aplicación podrás ver y guardar todos tus juegos favoritos de forma fácil y divertida!" +
                                    "\n\nEsta aplicación ha sido desarrollada por:" +
                                    "\n\t-Andrea Aparicio Porras (aaparicibh)" +
                                    "\n\t-Roberto Gamero Escudero (rgameroes)" +
                                    "\n\t-Jaime Pedro De Lope Santos (jlopesan)" +
                                    "\n\t-Alejandro Olmeda Díaz (aolmedad)");
                builder.setNegativeButton("Ok", null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        bfaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Preguntas frecuentes");
                builder.setMessage("¿Puedo usar la aplicación sin registrarme?" +
                        "\n\tPuedes buscar tus títulos favoritos sin necesidad de estar registrado. Sin embargo, algunas funcionalidades como añadir juegos a listas" +
                        "no están disponibles sin tener una cuenta." +
                        "\n\n¿Cómo puedo eliminar mi cuenta?" +
                        "\n\tTe vas a la pantalla de configuración y seleccionas la opción Eliminar cuenta." +
                        "\n\n¿Puedo usar varios filtros de búsqueda a la vez?" +
                        "\n\tLos filtros se pueden combinar de la forma que más se ajuste a tu búsqueda.");
                builder.setNegativeButton("Ok", null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });


        bdelAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Eliminar cuenta");
                builder.setMessage("¿Seguro que quieres eliminar tu cuenta? Esta acción no se podrá revertir");
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        long idUser = sp.getLong("UserID", -1);
                        configuracionViewModel.deleteUser(idUser);
                        sp.edit().remove("UserID").commit();

                        Toast toast = Toast.makeText(context, "Has borrado tu cuenta", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                        bdelAccount.setVisibility(View.INVISIBLE);
                        bEditarCuenta.setVisibility(View.INVISIBLE);
                        bIniSes.setVisibility(View.VISIBLE);
                        bReg.setVisibility(View.VISIBLE);
                    }
                });
                builder.setNegativeButton("Cancelar", null);

                AlertDialog dialog = builder.create();
                dialog.show();

        }
        });

        return root;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //View root = inflater.inflate(R.layout.fragment_perfil, container, false);

        final Button bEditarCuenta = getActivity().findViewById(R.id.editar_cuenta);
        final Button bIniSesButton = getActivity().findViewById(R.id.ini_ses_buttom);
        final Button bDelAccount = getActivity().findViewById(R.id.delAccount);
        final Button bRegAccount = getActivity().findViewById(R.id.regAccount);


        final SharedPreferences sp = getActivity().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        final long logged = sp.getLong("UserID", -1);
        if(logged != -1) {
            bEditarCuenta.setVisibility(View.VISIBLE);
            bDelAccount.setVisibility(View.VISIBLE);
            bIniSesButton.setVisibility(View.INVISIBLE);
            bRegAccount.setVisibility(View.INVISIBLE);
        }
    }

}
