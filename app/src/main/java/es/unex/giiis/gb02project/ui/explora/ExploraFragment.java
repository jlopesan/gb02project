package es.unex.giiis.gb02project.ui.explora;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.unex.giiis.gb02project.AppContainer;
import es.unex.giiis.gb02project.MyApplication;
import es.unex.giiis.gb02project.adapters.MyAdapter;
import es.unex.giiis.gb02project.R;
import es.unex.giiis.gb02project.dataApp.model.Platform_;
import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.dataApp.network.ResultNetworkLoaderRunnable;
import es.unex.giiis.gb02project.ui.search.SearchActivity;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.NewGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PopularGames;


public class ExploraFragment extends Fragment implements MyAdapter.OnListInteractionListener {

    public final static int SEARCH = 0;
    private ExploraViewModel exploraViewModel;
    private RecyclerView recyclerView;
    private MyAdapter pAdapter; //populares
    private MyAdapter nAdapter; //novedades
    private MyAdapter bAdapter; //busqueda
    private GridLayoutManager layoutManager;
    private TextView tSearchResult;

    private RecyclerView recyclerViewPopular;
    private GridLayoutManager layoutManagerPopular;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //exploraViewModel = ViewModelProviders.of(this).get(ExploraViewModel.class);
        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        exploraViewModel = new ViewModelProvider(this, appContainer.exploraFactory).get(ExploraViewModel.class);
        LifecycleOwner lifecycleOwner = this;
        View root = inflater.inflate(R.layout.fragment_explora, container, false);
       /*
        final TextView textView = root.findViewById(R.id.text_explora);
        exploraViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        */


        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view_explora);
        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this.getContext(), 3);
        recyclerView.setLayoutManager(layoutManager);

        pAdapter = new MyAdapter(new ArrayList<>(), this, getContext());
        nAdapter = new MyAdapter(new ArrayList<>(), this, getContext());
        bAdapter = new MyAdapter(new ArrayList<>(), this, getContext());

        Map<String, String> mParam = new HashMap<>();


        recyclerView.setAdapter(pAdapter);

       tSearchResult = root.findViewById(R.id.result_search_textview);
        ImageButton filterButton = root.findViewById(R.id.filterButton);
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                startActivityForResult(intent, SEARCH);
            }
        });

        Button bPopulares = root.findViewById(R.id.populares_button);

        bPopulares.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> mPopular = new HashMap<>();

                tSearchResult.setText(getString(R.string.result_search_text) + " " + getString(R.string.result_search_text_popular));
                //recyclerView.setAdapter(mAdapter);
                exploraViewModel.setParams(mPopular, 0); //tipo 0: populares y novedades
                recyclerView.setAdapter(pAdapter);
            }
        });

        exploraViewModel.getNew().observe(lifecycleOwner, new Observer<List<NewGames>>() {
            @Override
            public void onChanged(List<NewGames> newGames) {

                List<Result> resultsAux = new ArrayList<>();
                for(NewGames nG : newGames){
                    resultsAux.add(nG.toResult());

                }
                nAdapter.swap(resultsAux);
            }
        });

        exploraViewModel.getPopular().observe(lifecycleOwner, new Observer<List<PopularGames>>() {
            @Override
            public void onChanged(List<PopularGames> popularGames) {
                List<Result> resultsAux = new ArrayList<>();
                for(PopularGames pG : popularGames){
                    resultsAux.add(pG.toResult());
                }
                pAdapter.swap(resultsAux);
            }
        });

        exploraViewModel.getSearchGames().observe(lifecycleOwner, new Observer<List<Result>>() {
            @Override
            public void onChanged(List<Result> results) {
                bAdapter.swap(results);
            }
        });

        Button newsButton = root.findViewById(R.id.news_button);
        newsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar calendarResta = Calendar.getInstance();
                calendarResta.add(Calendar.MONTH, -1);
                Date fechaActual = new Date(Calendar.getInstance().getTimeInMillis());
                Date fechaAntigua = new Date(calendarResta.getTimeInMillis());

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String fechaAntiguaCadena = formatter.format(fechaAntigua);
                String fechaActualCadena = formatter.format(fechaActual);

                String fechasFinal = fechaAntiguaCadena+","+fechaActualCadena;

                mParam.put("dates", fechasFinal);

                tSearchResult.setText(getString(R.string.result_search_text) + " " + getString(R.string.news));
                exploraViewModel.setParams(mParam, 0); //tipo 0: populares y novedades
                recyclerView.setAdapter(nAdapter);
            }
        });
        return root;
    }

    @Override
    public void onListInteraction(String url) {
        Uri webpage = Uri.parse(url);
        Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
        startActivity(webIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK){
            tSearchResult.setText(getString(R.string.result_search_text) + " " + getString(R.string.result_search_text_custom));
            Map<String, String> searchQuery = (HashMap<String, String>) data.getSerializableExtra("search_query");
            //mAdapter.deleteData();

            exploraViewModel.setParams(searchQuery, 1); //tipo 1: búsqueda personalizada
            recyclerView.setAdapter(bAdapter);
            tSearchResult.setText(getString(R.string.result_search_text) + " " + getString(R.string.custom));
        }
    }
}