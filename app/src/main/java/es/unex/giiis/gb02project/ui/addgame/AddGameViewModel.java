package es.unex.giiis.gb02project.ui.addgame;


import androidx.lifecycle.ViewModel;

import es.unex.giiis.gb02project.dataApp.PerfilRepository;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.User;

public class AddGameViewModel extends ViewModel {

    private final PerfilRepository mRepository;

    public AddGameViewModel(PerfilRepository perfilRepository) {
        mRepository = perfilRepository;
    }

    public void insert(CustomGame customGame){
        mRepository.insertCustomGame(customGame);
    }
}
