package es.unex.giiis.gb02project.dataApp.network;

import java.util.Map;

import es.unex.giiis.gb02project.dataApp.model.*;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface RawgService {
        @GET("games")
        Call<Consulta> getConsultaJuegos();

        @GET("games/{id}")
        Call<Result> getConsultaJuegoID(@Path("id") String id);

        @GET("games")
        Call<Consulta> getConsultaParametros(@QueryMap Map<String, String> options);

        @GET("genres")
        Call<Consulta> getConsultaGeneros();

        @GET("platforms")
        Call<Consulta> getConsultaPlataformas();
}
