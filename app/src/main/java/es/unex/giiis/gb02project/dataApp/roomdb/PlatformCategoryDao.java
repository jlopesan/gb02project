package es.unex.giiis.gb02project.dataApp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PlatformCategory;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface PlatformCategoryDao {
    @Insert(onConflict = REPLACE)
    void bulkInsert(List<PlatformCategory> platformCategories);

    @Query("SELECT * FROM platform_category")
    public List<PlatformCategory> getAll();

    @Query("SELECT * FROM platform_category")
    public LiveData<List<PlatformCategory>> getLiveData();

    @Insert
    public void insert(PlatformCategory platformCategory);

    @Query("DELETE FROM platform_category")
    public void deleteAll();

    @Update
    public int update(PlatformCategory platformCategory);
}
