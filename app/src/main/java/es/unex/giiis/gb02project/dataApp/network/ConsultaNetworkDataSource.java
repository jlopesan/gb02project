package es.unex.giiis.gb02project.dataApp.network;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.gb02project.AppExecutors;
import es.unex.giiis.gb02project.dataApp.model.Consulta;
import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Genre;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PlatformCategory;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PopularGames;

public class ConsultaNetworkDataSource {
    private static final String LOG_TAG = ConsultaNetworkDataSource.class.getSimpleName();
    private static ConsultaNetworkDataSource sInstance;

    private final MutableLiveData<List<PlatformCategory>> mDownloadedPlatformCategories;
    private final MutableLiveData<List<Genre>> mDownloadedGenres;

    private ConsultaNetworkDataSource() {
        mDownloadedPlatformCategories = new MutableLiveData<>();
        mDownloadedGenres = new MutableLiveData<>();
    }

    public synchronized static ConsultaNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new ConsultaNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source");
        }
        return sInstance;
    }

    public LiveData<List<PlatformCategory>> getCurrentPlatformCategories() {
        return mDownloadedPlatformCategories;
    }

    public LiveData<List<Genre>> getCurrentGenres() {
        return mDownloadedGenres;
    }

    public void fetchPlatformCategories(){
        Log.d(LOG_TAG, "Fetch platforms started");
        // Get data from network and pass it to LiveData

        AppExecutors.getInstance().networkIO().execute(new ConsultaNetworkLoaderRunnable(false, new OnConsultaLoadedListener() {
            @Override
            public void onConsultaLoaded(Consulta consulta) {

                //resultsAux = results.toArray(new Result[
                List<PlatformCategory> pc = new ArrayList<>();
                for (Result r: consulta.getResults()){
                    pc.add(new PlatformCategory(r.getId(), r.getName()));
                }
                if(pc!=null) {
                    mDownloadedPlatformCategories.setValue(pc);
                }
            }
        }));
    }

    public void fetchGenres(){
        Log.d(LOG_TAG, "Fetch genres started");
        // Get data from network and pass it to LiveData

        AppExecutors.getInstance().networkIO().execute(new ConsultaNetworkLoaderRunnable(true, new OnConsultaLoadedListener() {
            @Override
            public void onConsultaLoaded(Consulta consulta) {

                //resultsAux = results.toArray(new Result[
                List<Genre> g = new ArrayList<>();
                for (Result r: consulta.getResults()){
                    g.add(new Genre(r.getId(), r.getName()));
                }
                if(g!=null) {
                    mDownloadedGenres.setValue(g);
                }
            }
        }));
    }
}
