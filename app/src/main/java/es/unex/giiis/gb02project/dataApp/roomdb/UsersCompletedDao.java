package es.unex.giiis.gb02project.dataApp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.PopularGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.User;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface UsersCompletedDao {

    @Insert(onConflict = REPLACE)
    void bulkInsert(List<UsersCompleted> completedGames);

    @Query("SELECT * FROM users_completed")
    public List<UsersCompleted> getAll();

    @Query("SELECT * FROM users_completed WHERE IDUser = :iduser ")
    public LiveData<List<UsersCompleted>> getLiveData(long iduser);

   // @Query("SELECT IDGame FROM users_completed WHERE IDUser = :iduser")
   // public List<Long> getGamesByUser(long iduser);

    @Query("SELECT * FROM users_completed WHERE IDUser = :iduser")
    public List<UsersCompleted> getGamesByUser(long iduser);

    @Query("SELECT * FROM users_completed WHERE IDUser = :iduser AND IDGame = :idgame")
    public UsersCompleted getRowByIds(long iduser, long idgame);

    @Insert
    public void insert(UsersCompleted usersCompleted);

    @Query("DELETE FROM users_completed")
    public void deleteAll();

    @Update
    public int update(UsersCompleted usersCompleted);

    @Query("DELETE FROM users_completed WHERE IDUser = :iduser AND IDGame = :idgame")
    public void deleteGameFromCompleted(long iduser, long idgame);
}
