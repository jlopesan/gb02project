package es.unex.giiis.gb02project.ui.signin;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.gb02project.dataApp.PerfilRepository;


public class SigninViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final PerfilRepository mRepository;

    public SigninViewModelFactory(PerfilRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new SigninViewModel(mRepository);
    }
}