package es.unex.giiis.gb02project.dataApp.roomdb.entities;


import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import es.unex.giiis.gb02project.dataApp.model.Result;

@Entity(tableName = "platform",primaryKeys = {"IDGame", "IDPlatform"})
public class Platform implements Serializable {

    @Ignore
    public final static Integer TipoPopular = 1;
    @Ignore
    public final static Integer TipoNovedad = 2;
    @Ignore
    public final static Integer TipoOtro = 0;

    @Ignore
    public final static String IDGame = "IDGame";
    @Ignore
    public final static String IDPlatform = "IDPlatform";
    @Ignore
    public final static String NAME = "name";
    @Ignore
    public final static String TYPE = "type";

    @SerializedName("mIDGame")
    @NonNull
    @ColumnInfo(name = "IDGame")
    private Integer mIDGame;

    @SerializedName("mIDPlatform")
    @NonNull
    @ColumnInfo(name = "IDPlatform")
    private Integer mIDPlatform;

    @SerializedName("mName")
    @ColumnInfo(name = "name")
    private String mName = new String();

    // Populares->1
    // Novedades->2
    // Listas user ->0
    @SerializedName("mType")
    @ColumnInfo(name = "TYPE")
    private Integer mType;

    public Platform(Integer mIDGame, Integer mIDPlatform, String mName, Integer mType) {
        this.mIDGame = mIDGame;
        this.mIDPlatform = mIDPlatform;
        this.mName = mName;
        this.mType = mType;
    }

    public Integer getIDGame() {
        return mIDGame;
    }

    public void setIDGame(Integer mIDGame) {
        this.mIDGame = mIDGame;
    }

    public Integer getIDPlatform() {
        return mIDPlatform;
    }

    public void setIDPlatform(Integer mIDPlatform) {
        this.mIDPlatform = mIDPlatform;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public Integer getType() {
        return mType;
    }

    public void setType(Integer mType) {
        this.mType = mType;
    }

    public static void packageIntent(Intent intent, Integer mIDGame, Integer mIDPlatform, String mName, Integer mType) {
        intent.putExtra(Platform.IDGame,mIDGame );
        intent.putExtra(Platform.IDPlatform, mIDPlatform);
        intent.putExtra(Platform.NAME, mName);
        intent.putExtra(Platform.TYPE, mType);

    }

    @Ignore
    public Platform(Intent intent) {
        mIDGame = intent.getIntExtra(Platform.IDGame,0);
        mIDPlatform = intent.getIntExtra(Platform.IDPlatform, 0);
        mName = intent.getStringExtra(Platform.NAME);
        mType = intent.getIntExtra(Platform.TYPE,0);
    }


    /////////////////////////////////////////

}